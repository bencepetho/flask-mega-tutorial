"""
Router module.
"""
from datetime import datetime
from typing import Union

import flask
from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

from app import app, db
from app.forms import EditProfileForm, LoginForm, RegistrationForm
from app.models import User


@app.route("/edit_profile", methods=["GET", "POST"])
@login_required
def edit_profile() -> Union[str, flask.Response]:
    """
    Render the edit profile page, or redirect to the same after making modifications.

    :return: The rendered HTML page or a redirect response.
    """
    form = EditProfileForm(current_user.username)

    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()

        flash("Your changes have been saved.")
        return redirect(url_for("edit_profile"))

    if request.method == "GET":
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me

    return render_template("edit_profile.html", title="Edit Profile", form=form)


@app.route("/")
@app.route("/index")
@login_required
def index() -> str:
    """
    Render the home page view.

    :return: The rendered HTML page.
    """
    posts = [
        {"author": {"username": "Yolo"}, "body": "Beautiful day in Reading!"},
        {"author": {"username": "Swag"}, "body": "The Black Widow movie was cool!"},
    ]
    return render_template("index.html", title="Home", posts=posts)


@app.route("/login", methods=["GET", "POST"])
def login() -> Union[str, flask.Response]:
    """
    Render the login page view, or redirect to the home page after logging in.

    :return: The rendered HTML page or a redirect response.
    """
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = LoginForm()
    if form.validate_on_submit():
        u = User.query.filter_by(username=form.username.data).first()

        if u is None or not u.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for("login"))

        login_user(u, form.remember_me.data)

        # Requests intercepted in views decorated by login_required will
        # redirect and add a 'next' query string argument to the URL
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("index")
        return redirect(next_page)

    return render_template("login.html", title="Sign In", form=form)


@app.route("/logout")
def logout() -> flask.Response:
    """
    Redirect to the home page after logging the current user out.

    :return: The redirect response.
    """
    logout_user()
    return redirect(url_for("index"))


@app.route("/register", methods=["GET", "POST"])
def register() -> Union[str, flask.Response]:
    """
    Redirect to the home page in case of an authenticated user, render the registration
    view otherwise then redirect to the login page after successful registration.

    :return: The rendered HTML page or a redirect response.
    """
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = RegistrationForm()
    if form.validate_on_submit():
        u = User(username=form.username.data, email=form.email.data)
        u.set_password(form.password.data)

        db.session.add(u)
        db.session.commit()

        flash("Congratulations, you are now a registered user!")
        return redirect(url_for("login"))

    return render_template("register.html", title="Register", form=form)


@app.route("/user/<username>")
@login_required
def user(username: str) -> str:
    """
    Render the user profile and it's posts.

    :param username: The user account to be displayed.
    :raise: HTTPException 404 if user not found.
    :return: The rendered HTML page.
    """
    u = User.query.filter_by(username=username).first_or_404()
    posts = [
        {"author": u, "body": "Test post #1"},
        {"author": u, "body": "Test post #2"},
    ]
    return render_template("user.html", user=u, posts=posts)


@app.before_request
def before_request() -> None:
    """
    Executes before every view function request.

    Update the Last Seen field of the current user in the database if authenticated.
    """
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
