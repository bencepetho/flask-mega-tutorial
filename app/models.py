"""
Database models module.
"""
from datetime import datetime
from hashlib import md5
from typing import Optional

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login


class User(UserMixin, db.Model):
    """
    A user of the application.
    """

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(128), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.Integer, default=datetime.utcnow)
    posts = db.relationship("Post", backref="author", lazy="dynamic")

    def __repr__(self) -> str:
        return f"<User {self.username}>"

    def set_password(self, password: str) -> None:
        """
        Generate and store the password hash for a user.

        :param password: The password to be hashed.
        """
        self.password_hash = generate_password_hash(password)

    def check_password(self, password: str) -> bool:
        """
        Validate the given password against the stored hash of a user.

        :param password: The password to be verified.
        :return: True if password is correct, False otherwise.
        """
        return check_password_hash(self.password_hash, password)

    def avatar(self, size: int) -> str:
        """
        Get the URL for the Gravatar image of a user based on their email address.

        If the user has no Gravatar image associated with their email address,
        a generated image will be served using a geometric pattern based on
        the email hash.

        :param size: The requested square image size in pixels.
        :return: The URL to request the avatar.
        """
        digest = md5(self.email.lower().encode("utf-8")).hexdigest()
        default = "identicon"
        return f"https://www.gravatar.com/avatar/{digest}?d={default}&s={size}"


class Post(db.Model):
    """
    A blog post of a user.
    """

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self) -> str:
        return f"<Post {self.body}>"


@login.user_loader
def load_user(user_id: str) -> Optional[User]:
    """
    Reload a user from the session.

    :param user_id: The ID of the user.
    :return: The user object or None if the user does not exist.
    """
    return User.query.get(int(user_id))
