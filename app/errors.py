"""
Error handler module.
"""
from flask import render_template

from app import app, db


@app.errorhandler(404)
def not_found_error(error: Exception) -> tuple[str, int]:
    """
    Render error page when resource is not found.

    :return: A tuple of the rendered HTML page and a 404 HTTP status code.
    """
    return render_template("404.html"), 404


@app.errorhandler(500)
def internal_error(error: Exception) -> tuple[str, int]:
    """
    Rollback db transaction and render error page when internal server error happens.

    :return: A tuple of the rendered HTML page and a 500 HTTP status code.
    """
    db.session.rollback()
    return render_template("500.html"), 500
