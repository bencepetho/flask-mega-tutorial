"""
Web form handler module.
"""
from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email, EqualTo, Length, ValidationError

from app.models import User


class EditProfileForm(FlaskForm):
    """
    Web form to handle user profile edits.
    """

    username = StringField("Username", validators=[DataRequired()])
    about_me = TextAreaField("About Me", validators=[Length(min=0, max=140)])
    submit = SubmitField("Submit")

    def __init__(self, original_username, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username: StringField) -> None:
        """
        In-line validator that the given username is not yet taken.

        :param username: Desired username form field.
        :raise: ValidationError if the username is already taken.
        """

        if username.data != self.original_username:
            user = User.query.filter_by(username=username.data).first()

            if user is not None:
                raise ValidationError("Username is taken.")


class LoginForm(FlaskForm):
    """
    Web form to handle logins.
    """

    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign In")


class RegistrationForm(FlaskForm):
    """
    Web form to handle user registrations.
    """

    username = StringField("Username", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    password_repeat = PasswordField(
        "Repeat Password", validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField("Register")

    def validate_username(self, username: StringField) -> None:
        """
        In-line validator that the given username is not yet taken.

        :param username: Desired username form field.
        :raise: ValidationError if the username is already taken.
        """
        user = User.query.filter_by(username=username.data).first()

        if user is not None:
            raise ValidationError("Username is taken.")

    def validate_email(self, email: StringField) -> None:
        """
        In-line validator that the given email is not yet registered.

        :param email: Email address form field to be registered.
        :raise: ValidationError if the email address is already registered.
        """
        user = User.query.filter_by(email=email.data).first()

        if user is not None:
            raise ValidationError("Email address is already registered.")
