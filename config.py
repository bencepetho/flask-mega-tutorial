"""
Application configuration module.
"""
import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    """
    The main flask application configuration object.
    """

    # Configuration of the logging SMTP handler and the email recipients.
    ADMINS = ["yolo@example.com"]
    MAIL_SERVER = os.environ.get("MAIL_SERVER")
    MAIL_PORT = int(os.environ.get("MAIL_PORT") or 25)
    MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS") is not None
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")

    # A secret key that will be used for securely signing the session cookie and can
    # be used for any other security related needs by extensions or your application.
    # https://flask.palletsprojects.com/en/2.0.x/config/#SECRET_KEY
    SECRET_KEY = os.environ.get("SECRET_KEY") or "you-will-never-guess"

    # The database URI that should be used for the connection.
    # https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/#configuration-keys
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "DATABASE_URL"
    ) or "sqlite:///" + os.path.join(basedir, "app.db")

    # If set to True, Flask-SQLAlchemy will track modifications of objects and emit signals.
    # https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/#configuration-keys
    SQLALCHEMY_TRACK_MODIFICATIONS = False
