"""
The main application module.
"""
from app import app, db
from app.models import User, Post


@app.shell_context_processor
def make_shell_context() -> dict:
    """
    Register a shell context processor function to import symbols into a flask shell.

    :return: The symbols imported into a shell context.
    """
    return {"db": db, "User": User, "Post": Post}


if __name__ == "__main__":
    app.run(debug=True)
